import 'package:flutter/material.dart';

class CurvedPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.white
      ..strokeWidth = 15;

    var path = Path();

    path.moveTo(0, size.height * 0.9);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.9,
        size.width * 1.0, size.height * 0.8);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CurvedPaintersection extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.white
      ..strokeWidth = 15;

    var path = Path();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint0 = Paint()
      ..color = const Color(0xffE6FFFA)

      ..strokeWidth = 1;

    Path path0 = Path();
    path0.moveTo(size.width * 0.8962500, size.height * 1.5733333);
    path0.lineTo(0, size.height * 0.8900000);
    path0.lineTo(size.width * 0.3237500, size.height * 0.013333);
    path0.lineTo(size.width * 0.6562500, size.height * 0.0500000);
    path0.lineTo(size.width * 0.8875000, size.height * 0.0500000);
    path0.lineTo(size.width, size.height * 0.0466667);
    path0.lineTo(size.width, size.height * 0.8133333);
    path0.lineTo(size.width * 0.7512500, size.height * 0.9033333);
    path0.lineTo(size.width * 0.4762500, size.height * 0.8733333);
    path0.lineTo(size.width * 0.1562500, size.height * 0.8900000);
    path0.lineTo(0, size.height * 0.8933333);
    path0.lineTo(0, size.height * 0.3266667);
    path0.lineTo(0, 0);
    path0.lineTo(size.width * 0.3237500, size.height * 0.0166667);
    path0.lineTo(size.width * 0.3237500, size.height * 0.0200000);
    path0.lineTo(size.width * 0.0012500, size.height * 0.8933333);
    path0.lineTo(0, size.height * 0.8833333);
    path0.lineTo(size.width * 0.0037500, size.height * 0.8933333);

    canvas.drawPath(path0, paint0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

