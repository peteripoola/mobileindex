import 'package:flutter/material.dart';
import 'home screen/mobileview.dart';
import 'home screen/webview.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Test Project',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LayoutBuilder(builder: ((context, constraints) {
         

          if (constraints.maxWidth > 1200) {
            //webview component
              return const  Webview();
          }
           else if (constraints.maxWidth > 800 &&
              constraints.maxWidth < 1200) {
            return const Webview();
          } else {
            //Mobile view component
            return const Mobileview();
          }
        })));
  }
}
