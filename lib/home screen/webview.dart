import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileindex/home%20screen/util/home_screen_util.dart';
import '../Navbar/navbar.dart';
import '../util/paint.dart';

class Webview extends StatefulWidget {
  const Webview({Key? key}) : super(key: key);

  @override
  State<Webview> createState() => _WebviewState();
}

class _WebviewState extends State<Webview> {
  String option = 'Arbeitnehmer';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.topCenter,
              //height: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0xffEBF4FF).withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 8,
                    offset: const Offset(0, 3), //
                  )
                ],
              ),
              child: const Navbar(),
            ),
            Container(
              height: 359,
              color: const Color(0xffEBF4FF),
              child: Stack(
                children: [
                  CustomPaint(
                    size: MediaQuery.of(context).size,
                    // MediaQuery.of(context).size,
                    painter: CurvedPainter(),
                  ),
                  const SizedBox(
                    height: 29,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const TopSectionContent(),
                          const SizedBox(
                            height: 53,
                          ),
                          MaterialButton(
                            height: 40,
                            minWidth: 260,
                            onPressed: () {},
                            color: Colors.blueAccent,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12))),
                            child: Text(
                              'Kostenlos Registrieren',
                              style: GoogleFonts.lato(
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: 255,
                        width: 255,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/agreementmobile.png')),
                            color: Colors.white,
                            shape: BoxShape.circle),
                      ),
                      const Text("")
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Container(
                height: 40,
                width: 480,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  border: Border(
                    top: BorderSide(color: Color(0xffCBD5E0)),
                    bottom: BorderSide(color: Color(0xffCBD5E0)),
                    left: BorderSide(color: Color(0xffCBD5E0)),
                    right: BorderSide(color: Color(0xffCBD5E0)),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            option = "Arbeitnehmer";
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: option == 'Arbeitnehmer'
                                ? const Color(0xff81E6D9)
                                : Colors.white,
                            borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                            ),
                          ),
                          child: Text(
                            'Arbeitnehmer',
                            style: GoogleFonts.lato(
                              color: const Color(0xff2D3748),
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            option = "Arbeitgeber";
                          });
                        },
                        child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: option == 'Arbeitgeber'
                                  ? const Color(0xff81E6D9)
                                  : Colors.white,
                              border: const Border(
                                left: BorderSide(color: Color(0xffCBD5E0)),
                                right: BorderSide(color: Color(0xffCBD5E0)),
                              ),
                            ),
                            child: Text('Arbeitgeber',
                                style: GoogleFonts.lato(
                                  color: const Color(0xff2D3748),
                                  fontSize: 14,
                                ))),
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            option = "Temporärbüro";
                          });
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: option == 'Temporärbüro'
                                ? const Color(0xff81E6D9)
                                : Colors.white,
                            borderRadius: const BorderRadius.only(
                                bottomRight: Radius.circular(10),
                                topRight: Radius.circular(10)),
                          ),
                          child: Text('Temporärbüro',
                              style: GoogleFonts.lato(
                                color: const Color(0xff2D3748),
                                fontSize: 14,
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            if (option == 'Arbeitnehmer')
              TabSectionTopComponent(
                  label: 'Drei einfache Schritte \nzu deinem neuen Mitarbeiter')
            else if (option == 'Arbeitgeber')
              TabSectionTopComponent(
                  label: 'Drei einfache Schritte zu \ndeinem neuen Mitarbeiter')
            else if (option == 'Temporärbüro')
              TabSectionTopComponent(
                  label:
                      'Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter')
            else
              Container(),
            option == 'Arbeitnehmer'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              SizedBox(
                                child: Row(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      height: 78,
                                      width: 78,
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Color(0xffF7FAFC)),
                                      child: Text(
                                        '1.',
                                        style: GoogleFonts.lato(
                                            fontSize: 40,
                                            color: const Color(0xff718096)),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 25.0,
                                      ),
                                      child: Text(
                                        'Erstellen dein Lebenslauf',
                                        style: GoogleFonts.lato(
                                            fontSize: 17,
                                            color: const Color(0xff718096)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 70,
                      ),
                      SizedBox(
                          width: 200,
                          height: 200,
                          child: Image.asset('assets/profile.png')),
                    ],
                  )
                : Container(),
            option == 'Arbeitnehmer'
                ? Container(
                    height: 370,
                    color: Colors.white,
                    child: Stack(children: [
                      SizedBox(
                        height: 300,
                        width: MediaQuery.of(context).size.width,
                        child: CustomPaint(
                          size: MediaQuery.of(context).size,
                          painter: RPSCustomPainter(),
                          child: SizedBox(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                    height: 250,
                                    width: 250,
                                    child: Image.asset('assets/person.png')),
                                const SizedBox(
                                  width: 40,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            '2.',
                                            style: GoogleFonts.lato(
                                                fontSize: 40,
                                                color: const Color(0xff718096)),
                                          ),
                                          const SizedBox(
                                            width: 15,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 20.0,
                                            ),
                                            child: Text(
                                              'Erstellen dein Lebenslauf',
                                              style: GoogleFonts.lato(
                                                  fontSize: 17,
                                                  color:
                                                      const Color(0xff718096)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ]),
                  )
                : Container(),
            option == 'Arbeitnehmer'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                alignment: Alignment.topCenter,
                                height: 154,
                                width: 154,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xffF7FAFC)),
                                child: Text(
                                  '3.',
                                  style: GoogleFonts.lato(
                                      fontSize: 40,
                                      color: const Color(0xff718096)),
                                ),
                              ),
                              Positioned(
                                top: 20,
                                left: 100,
                                child: Text(
                                  'Mit nur einem Klick \nbewerben',
                                  style: GoogleFonts.lato(
                                      fontSize: 17,
                                      color: const Color(0xff718096)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 170,
                      ),
                      SizedBox(
                          width: 250,
                          height: 250,
                          child: Image.asset('assets/section.png')),
                    ],
                  )
                : Container(),
            option == 'Arbeitgeber'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 78,
                                width: 78,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xffF7FAFC)),
                              ),

                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 20.0, left: 25),
                                child: Text(
                                  '1.',
                                  style: GoogleFonts.lato(
                                      fontSize: 40,
                                      color: const Color(0xff718096)),
                                ),
                              )
                              //),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25.0,
                            ),
                            child: Text(
                              'Erstellen dein Lebenslauf',
                              style: GoogleFonts.lato(
                                  fontSize: 17, color: const Color(0xff718096)),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 70,
                      ),
                      SizedBox(
                          width: 200,
                          height: 200,
                          child: Image.asset('assets/profile.png')),
                      const SizedBox()
                    ],
                  )
                : Container(),
            const SizedBox(
              height: 30,
            ),
            option == 'Arbeitgeber'
                ? Container(
                    height: 370,
                    color: Colors.white,
                    child: Stack(children: [
                      CustomPaint(
                        size: MediaQuery.of(context).size,
                        painter: RPSCustomPainter(),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 250,
                            width: 250,
                            child:
                                Image.asset('assets/undraw_about_me_web.png'),
                          ),
                          const SizedBox(
                            width: 40,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                decoration:
                                    const BoxDecoration(shape: BoxShape.circle),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      '2.',
                                      style: GoogleFonts.lato(
                                          fontSize: 40,
                                          color: const Color(0xff718096)),
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 20.0,
                                      ),
                                      child: Text(
                                        'Erstellen dein Lebenslauf',
                                        style: GoogleFonts.lato(
                                            fontSize: 17,
                                            color: const Color(0xff718096)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ]),
                  )
                : Container(),
            const SizedBox(
              height: 40,
            ),
            option == 'Arbeitgeber'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 25, top: 20),
                                          child: Text(
                                            '3.',
                                            style: GoogleFonts.lato(
                                                fontSize: 40,
                                                color: const Color(0xff718096)),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 20.0,
                                          ),
                                          child: Text(
                                            'Mit nur einem Klick \nbewerben',
                                            style: GoogleFonts.lato(
                                                fontSize: 17,
                                                color: const Color(0xff718096)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 70,
                      ),
                      SizedBox(
                        width: 250,
                        height: 250,
                        child: Image.asset('assets/undraw_swipe.png'),
                      ),
                    ],
                  )
                : Container(),
            option == 'Temporärbüro'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 78,
                                width: 78,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xffF7FAFC)),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 20.0, left: 25),
                                child: Text(
                                  '1.',
                                  style: GoogleFonts.lato(
                                      fontSize: 40,
                                      color: const Color(0xff718096)),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 25.0,
                            ),
                            child: Text(
                              'Erstellen dein Lebenslauf',
                              style: GoogleFonts.lato(
                                  fontSize: 17, color: const Color(0xff718096)),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 70,
                      ),
                      SizedBox(
                          width: 200,
                          height: 200,
                          child: Image.asset('assets/profile.png')),
                      const SizedBox()
                    ],
                  )
                : Container(),
            // const SizedBox(
            //   height: 30,
            // ),
            option == 'Temporärbüro'
                ? Container(
                    height: 370,
                    color: Colors.white,
                    child: Stack(children: [
                      CustomPaint(
                        size: MediaQuery.of(context).size,
                        painter: RPSCustomPainter(),
                      ),
                      const SizedBox(
                        height: 90,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                              height: 250,
                              width: 250,
                              child:
                                  Image.asset('assets/undraw_job_offers.png')),
                          const SizedBox(
                            width: 40,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                decoration:
                                    const BoxDecoration(shape: BoxShape.circle),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      '2.',
                                      style: GoogleFonts.lato(
                                          fontSize: 40,
                                          color: const Color(0xff718096)),
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        top: 20.0,
                                      ),
                                      child: Text(
                                        'Erstellen dein Lebenslauf',
                                        style: GoogleFonts.lato(
                                            fontSize: 17,
                                            color: const Color(0xff718096)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ]),
                  )
                : Container(),
            const SizedBox(
              height: 40,
            ),
            option == 'Temporärbüro'
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 25, top: 20),
                                          child: Text(
                                            '3.',
                                            style: GoogleFonts.lato(
                                                fontSize: 40,
                                                color: const Color(0xff718096)),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                            top: 20.0,
                                          ),
                                          child: Text(
                                            'Mit nur einem Klick \nbewerben',
                                            style: GoogleFonts.lato(
                                                fontSize: 17,
                                                color: const Color(0xff718096)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 70,
                      ),
                      SizedBox(
                          width: 250,
                          height: 250,
                          child:
                              Image.asset('assets/undraw_business_deal.png')),
                    ],
                  )
                : Container()
          ],
        ),
      ),
    ));
  }
}

// ignore: must_be_immutable
class TabSectionImageComponentOneWeb extends StatelessWidget {
  String label;
  String imageurl;

  TabSectionImageComponentOneWeb({
    required this.label,
    required this.imageurl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 78,
                    width: 78,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle, color: Color(0xffF7FAFC)),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, left: 25),
                    child: Text(
                      label,
                      style: GoogleFonts.lato(
                          fontSize: 40, color: const Color(0xff718096)),
                    ),
                  )
                  //),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 25.0,
                ),
                child: Text(
                  imageurl,
                  style: GoogleFonts.lato(
                      fontSize: 17, color: const Color(0xff718096)),
                ),
              ),
            ],
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.end,
          //   children: [
          //     Text(
          //       '1. ',
          //       style: GoogleFonts.lato(
          //         color: const Color(0xff718096),
          //         fontSize: 80,
          //       ),
          //     ),
          //     Column(
          //       crossAxisAlignment: CrossAxisAlignment.start,
          //       children: [
          //         Expanded(
          //           child: SizedBox(
          //             width: 200,
          //             // height: 200,
          //             child: Image.asset(imageurl),
          //           ),
          //         ),
          //         Padding(
          //           padding: const EdgeInsets.only(bottom: 18.0),
          //           child: Text(
          //             label,
          //             style: GoogleFonts.lato(
          //                 fontSize: 17, color: const Color(0xff718096)),
          //           ),
          //         )
          //       ],
          //     ),
          //     const SizedBox(
          //       height: 0,
          //     ),
        ],
      ),
    );
  }
}
