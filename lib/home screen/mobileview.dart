import 'package:flutter/material.dart';
import 'package:mobileindex/home%20screen/util/home_screen_util.dart';

class Mobileview extends StatefulWidget {
  const Mobileview({Key? key}) : super(key: key);

  @override
  State<Mobileview> createState() => _MobileviewState();
}

class _MobileviewState extends State<Mobileview> {
  late ScrollController _scrollController;
  String option = 'Arbeitnehmer';

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: const Duration(seconds: 5), curve: Curves.linear);
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomSheet: Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12), topRight: Radius.circular(12)),
        ),
        child: Container(
          margin: const EdgeInsets.all(25),
          child: ElevatedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
              ),
              elevation: MaterialStateProperty.all(0.0),
              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.disabled)) {
                    return Colors.grey;
                  }
                  return Colors.blueAccent;
                },
              ),
              textStyle: MaterialStateProperty.all(
                const TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            onPressed: _scrollToTop,
            child: const Text('Kostenlos Registrieren'),
          ),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: [
              //login and app bar section
              const AppbarSectionComponent(),
              Container(
                color: const Color(0xffE6FFFA),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    const Center(
                      child: TopSectionContent(),
                    ),
                    const SizedBox(
                      height: 53,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(),
                      child: Image.asset(
                        'assets/agreementweb.png',
                      ),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  height: 40,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    border: Border(
                      top: BorderSide(color: Color(0xffCBD5E0)),
                      bottom: BorderSide(color: Color(0xffCBD5E0)),
                      left: BorderSide(color: Color(0xffCBD5E0)),
                      right: BorderSide(color: Color(0xffCBD5E0)),
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              option = "Arbeitnehmer";
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: option == 'Arbeitnehmer'
                                  ? const Color(0xff81E6D9)
                                  : Colors.white,
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                              ),
                            ),
                            child: const Text('Arbeitnehmer'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              option = "Arbeitgeber";
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: option == 'Arbeitgeber'
                                  ? const Color(0xff81E6D9)
                                  : Colors.white,
                              border: const Border(
                                left: BorderSide(color: Color(0xffCBD5E0)),
                                right: BorderSide(color: Color(0xffCBD5E0)),
                              ),
                            ),
                            child: const Text('Arbeitgeber'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              option = "Temporärbüro";
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: option == 'Temporärbüro'
                                  ? const Color(0xff81E6D9)
                                  : Colors.white,
                              borderRadius: const BorderRadius.only(
                                  bottomRight: Radius.circular(10),
                                  topRight: Radius.circular(10)),
                            ),
                            child: const Text('Temporärbüro'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              if (option == 'Arbeitnehmer')
                TabSectionTopComponent(
                    label:
                        'Drei einfache Schritte \nzu deinem neuen Mitarbeiter')
              else if (option == 'Arbeitgeber')
                TabSectionTopComponent(
                    label:
                        'Drei einfache Schritte zu \ndeinem neuen Mitarbeiter')
              else if (option == 'Temporärbüro')
                TabSectionTopComponent(
                    label:
                        'Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter')
              else
                Container(),
              if (option == 'Arbeitnehmer')
                TabSectionImageComponentOne(
                  label: 'Erstellen dein Lebenslauf',
                  imageurl: 'assets/profile.png',
                )
              else if (option == 'Arbeitgeber')
                TabSectionImageComponentOne(
                  label: 'Erstellen dein \nUnternegmensprofil',
                  imageurl: 'assets/profile.png',
                )
              else if (option == 'Temporärbüro')
                TabSectionImageComponentOne(
                  label: 'Erstellen dein \nUnternehmensprofil',
                  imageurl: 'assets/profile.png',
                )
              else
                Container(),

              //Job description second row
              if (option == 'Arbeitnehmer')
                TabSectionImageComponentTwo(
                  label: 'Erstellen dein Lebenslauf',
                  imageurl: 'assets/person.png',
                )
              else if (option == 'Arbeitgeber')
                TabSectionImageComponentTwo(
                    label: 'Erstellen ein Jobinserat',
                    imageurl: 'undraw_about_me_web.png'
                    //'assets/undraw_about_me.png',
                    )
              else if (option == 'Temporärbüro')
                TabSectionImageComponentTwo(
                  label: 'Erhalte Vermittlungs- \nangebot von Arbeitgeber',
                  imageurl: 'assets/undraw_job_offers.png',
                )
              else
                Container(),
              //Job description third row

              if (option == 'Arbeitnehmer')
                TabSectionImageComponentThree(
                  label: 'Erstellen dein Lebenslauf',
                  imageurl: 'assets/section.png',
                )
              else if (option == 'Arbeitgeber')
                TabSectionImageComponentThree(
                  label: 'Erstellen ein Jobinserat',
                  imageurl: 'assets/undraw_swipe.png',
                )
              else if (option == 'Temporärbüro')
                TabSectionImageComponentThree(
                  label: 'Erhalte Vermittlungs- \nangebot von Arbeitgeber',
                  imageurl: 'assets/undraw_business_deal.png',
                )
              else
                Container(),

              const SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }
}
