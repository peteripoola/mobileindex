import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileindex/Navbar/navbar.dart';

import '../../util/paint.dart';

class AppbarSectionComponent extends StatelessWidget {
  const AppbarSectionComponent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
        boxShadow: [
          BoxShadow(
            color: const Color(0xffEBF4FF).withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 8,
            offset: const Offset(0, 3), //
          )
        ],
      ),
      child: const Navbar(),
    );
  }
}

// ignore: must_be_immutable
class TabSectionImageComponentThree extends StatelessWidget {
  String label;
  String imageurl;
  TabSectionImageComponentThree({
    required this.label,
    required this.imageurl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.topCenter,
            height: 154,
            width: 154,
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: Color(0xffF7FAFC)),
            child: Text(
              '3.',
              style: GoogleFonts.lato(
                  fontSize: 40, color: const Color(0xff718096)),
            ),
          ),
          // Text(
          //   '3. ',
          //   style: GoogleFonts.lato(
          //     color: const Color(0xff718096),
          //     fontSize: 80,
          //   ),
          // ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Text(
                  label,
                  style: GoogleFonts.lato(
                      fontSize: 17, color: const Color(0xff718096)),
                ),
              ),
              Expanded(
                child: SizedBox(
                  width: 200,
                  // height: 200,
                  child: Image.asset(imageurl),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class TabSectionImageComponentTwo extends StatelessWidget {
  String label;
  String imageurl;
  TabSectionImageComponentTwo({
    required this.label,
    required this.imageurl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 370,
      color: Colors.white,
      child: Stack(children: [
        CustomPaint(
          size: MediaQuery.of(context).size,
          painter: RPSCustomPainter(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '2. ',
              style: GoogleFonts.lato(
                color: const Color(0xff718096),
                fontSize: 80,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 70.0),
                  child: Text(
                    label,
                    style: GoogleFonts.lato(
                        fontSize: 17, color: const Color(0xff718096)),
                  ),
                ),
                SizedBox(
                  width: 200,
                  height: 200,
                  child: Image.asset(
                    imageurl,
                  ),
                ),
              ],
            )
          ],
        ),
      ]),
    );
  }
}

// ignore: must_be_immutable
class TabSectionImageComponentOne extends StatelessWidget {
  String label;
  String imageurl;

  TabSectionImageComponentOne({
    required this.label,
    required this.imageurl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            '1. ',
            style: GoogleFonts.lato(
              color: const Color(0xff718096),
              fontSize: 80,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SizedBox(
                  width: 200,
                  // height: 200,
                  child: Image.asset(imageurl),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 18.0),
                child: Text(
                  label,
                  style: GoogleFonts.lato(
                      fontSize: 17, color: const Color(0xff718096)),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 0,
          ),
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class TabSectionTopComponent extends StatelessWidget {
  String label;

  TabSectionTopComponent({
    required this.label,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        textAlign: TextAlign.center,
        label,
        style: GoogleFonts.lato(
          color: const Color(0xff2D3748),
          fontSize: 25,
        ),
      ),
    );
  }
}

class TopSectionContent extends StatelessWidget {
  const TopSectionContent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      "Deine Job \nwebsite",
      textAlign: TextAlign.center,
      style: GoogleFonts.lato(
        color: const Color(0xff2D3748),
        fontSize: 45,
      ),
    );
  }
}
